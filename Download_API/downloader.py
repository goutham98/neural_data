###############################################################################
#################### NEURAL MARKER DATA DOWNLOAD API CODE #####################
###############################################################################
from .annotations_writer import Annotation_Writer
import threading
import numpy as np
import urllib.request
import json
import requests
import shutil
import time
import sys
import os
from typing import List, Tuple, Dict, Any, Set

TYPE_OF_DATA = {
    0 : "Rectangle",
    1 : "Segmentation",
    2 : "Classification",
    3 : "Keypoints"
}
                                    
class Downloader:
    def __init__(self, login_token: str, config: Dict[str, Any]):
        self.config = config
        self.login_token = login_token
        self.dataset_name, self.datasets = self.get_dataset_name()
        self.dataset_query_id = "id" if self.dataset_name_typ == int else "name"
        self.type_of_data = TYPE_OF_DATA[self.config["TASK_TYPE"]]
        self.annotation_format = self.config["ANNOTATION_FORMAT"]
        root_dir = os.sep.join(os.path.dirname(os.path.abspath(__file__)).split(os.sep)[:-2])
        self.DATASET_FETCH_URL = self.config['NEURAL_MARKER_SERVER_URL_PREFIX'] + \
                                    self.config['DATASET_FETCH_ENDPOINT']
        self.save_dir = os.path.join(root_dir, str(self.config['USER_ID']) + '_' +str(self.dataset_name))
        self.images_dir = os.path.join(self.save_dir,'images')
        self.train_images_dir = os.path.join(self.save_dir, 'train')
        self.val_images_dir = os.path.join(self.save_dir, 'val')
        self.annots_dir = os.path.join(self.save_dir, self.config['ANNOTATIONS_PATH_NAME'])
        self.annot_writer = Annotation_Writer(self.annotation_format,
                                self.annots_dir, self.type_of_data,
                                self.dataset_name, self.config['VAL_RATIO'])
        self.merge_categories = self.config["MERGE_CATEGORIES"]
        self.use_dataset_api = self.config["USE_DATASET_API"]
        if not self.use_dataset_api: assert self.dataset_query_id == "id", "When providing dataset name cannot use newer dataset api endpoint code"
        self.max_images = -1 if ((not self.config["MAX_IMAGES"]) or self.config["MAX_IMAGES"] < 1) else self.config["MAX_IMAGES"]
        # Below Parameters states the fetched dataset properties
        self.lock = threading.Lock()
        self.skip_count = 0
        self.fetched_datasets = None
        self.aws_dataset_directory = None
        self.oid = None

    def start_download(self):
        print('Started images download')
        self.fetched_datasets = self.get_datasets()
        self.filtered_datasets = self.get_filtered_dataset_dict()
        self.aws_dataset_directory = [info[1]['directory'] for info in self.filtered_datasets]
        self.oid = [info[1]['organisation_id']['$oid'] for info in self.filtered_datasets]
        self.IMAGES_FETCH_URLS = self.get_images_fetch_url()
        self.images_responses_dict = self.get_images(self.config['DATASET_INIT_PAGE'])
        self.num_of_pages = [int(resp['pages']) for resp in self.images_responses_dict]
        all_possible_categories = [resp['categories'] for resp in self.images_responses_dict]
        self.category_dict, self.ind_category_dict = self.merge_classes(all_possible_categories, strict=self.config["STRICT_CLASSES"])
        print("All categories are {}".format(", ".join(self.category_dict.keys())))
        self.annot_writer.writer.populate_category_ids(self.category_dict, self.ind_category_dict, self.merge_categories)
        
        self._create_save_and_dataset_dir()
        for num in range(len(self.images_responses_dict)):
            self.downloaded_count = 0
            print("Downloading Dataset {}".format(self.filtered_datasets[num][1]["name"]))
            self.current_fetch_url = self.IMAGES_FETCH_URLS[num]   
            self.cur_proc = num #indicates the current dataset in processing   
            if self.config['USE_MULTITHREADING']:
                self.start_multiprocess_download(self.num_of_pages[num])
            else:
                self.start_normal_download(self.num_of_pages[num])
        if self.config['FIX_SEED']:
            seed = 100
        else:
            seed = None
        print('Splitting the downloaded files into train and split')
        self.annot_writer.writer.split_and_write(self.annots_dir,self.images_dir,self.train_images_dir, self.val_images_dir,self.filtered_datasets,self.skip_count, seed)
        print('Dataset download completed')
    
    def get_dataset_name(self) -> Tuple[str, List]:
        if type(self.config["DATASET_NAME"]) in [str, int]:
            self.dataset_name_typ = type(self.config["DATASET_NAME"])
            return self.config["DATASET_NAME"], [self.config["DATASET_NAME"]]
        elif type(self.config["DATASET_NAME"]) == list:
            if len(self.config["DATASET_NAME"]) == 0:
                raise ValueError("Empty list provided for dataset!")
            elif len(self.config["DATASET_NAME"]) == 1:
                self.dataset_name_typ = type(self.config["DATASET_NAME"][0])
                return self.config["DATASET_NAME"][0], self.config["DATASET_NAME"]
            else:
                self.dataset_name_typ = type(self.config["DATASET_NAME"][0])
                assert all(isinstance(i, self.dataset_name_typ) for i in self.config["DATASET_NAME"]), "All elements of DATASET NAME should be of same type(str or int)"
                return self.config["DEFAULT_MERGE_NAME"], self.config["DATASET_NAME"]

    def get_images_fetch_url(self) -> List[str]:
        
        suffixes = [self.config['IMAGES_URL_SUFFIX'].replace(
                                "DID", str(info[1]['id'])) for info in self.filtered_datasets]
        images_abs_url_paths = [os.path.join(self.DATASET_FETCH_URL, pa) for pa in suffixes]
        return images_abs_url_paths

    def merge_classes(self, category_list: List[List[Dict]], strict: bool):
        final_categories = []
        ind_classes = []
        for cat in category_list:
            ind_classes.append(tuple([i["name"] for i in cat]))
        classes_set = set(ind_classes)
        if strict and (len(classes_set) !=1):
            raise ValueError("Datasets having different num of classes in strict mode")
        common_class_map, ind_class_map = self.merge_class_names(ind_classes, category_list)
        for i, k in enumerate(category_list):
            assert len(k) == len(ind_class_map[i]), "Something wrong in class mapping"
        return common_class_map, ind_class_map

    def merge_class_names(self, category_list: List[Tuple], org_response: List[List[Dict]]) -> (Dict[str, Dict], List[Dict[int, str]]):
        all_classes = set()
        for cat in category_list:
            all_classes = all_classes.union(cat)
        id_mapping = {}
        original_mapping = []
        for i,j in enumerate(all_classes):
            id_mapping[j] = {"id": i+1, "super_category": "all"}
        for cat in org_response:
            temp = {}
            for ins in cat:
                temp[ins['id']] =  ins['name']
            original_mapping.append(temp)
        return id_mapping, original_mapping

    def start_normal_download(self, num_of_pages):
        for page_number in range(1, num_of_pages + 1):
            image_response_dict = self.get_page_images(page_number)
            for image_dict in image_response_dict['images']:
                if image_dict['annotated'] or image_dict['skip_details']['is_skipped']:
                    image_url = self.download_the_image(image_dict)
                    self.annot_writer.writer.write_annotation(image_dict ,image_url, cur_proc)
            
                
    def download_the_image(self, image_dict: Dict[str, Any]) -> str:
        image_id = image_dict['id']
        filename = image_dict['file_name']
        #print("Downloading: {}, {}".format(image_id, filename))
        image_ext = os.path.splitext(filename)[1]
        image_url = self.config['AWS_BUCKET_URL_PREFIX_LINK'] \
                    + self.aws_dataset_directory[self.cur_proc] + image_id + image_ext
        download_image_path = os.path.join(self.images_dir, filename)
        urllib.request.urlretrieve(image_url, download_image_path)
        return image_url

    def get_images(self, page_number: int) -> List[Dict]:
        limit_page_str = "?limit={}&page={}&route=dataset".format(
                                self.config['DATASET_LIMIT'],
                                page_number)
        temp_urls = [url + limit_page_str for url in self.IMAGES_FETCH_URLS]
        payload = {}
        files = {}
        headers = {
            'token': self.login_token
        }
        responses = []
        for ins in temp_urls:
            images_response = requests.request("GET", ins, headers=headers, 
                                            data = payload, files = files)
            responses.append(json.loads(images_response.text))
        return responses

    def get_page_images(self, page_number: int) -> Dict:
        limit_page_str = "?limit={}&page={}&route=dataset".format(
                                self.config['DATASET_LIMIT'],
                                page_number)
        temp_url = self.current_fetch_url + limit_page_str
        payload = {}
        files = {}
        headers = {
            'token': self.login_token
        }
        responses = []
        images_response = requests.request("GET", temp_url, headers=headers, 
                                        data = payload, files = files)
        return json.loads(images_response.text)
        
    def multiprocess_download_func(self, start_page: int, end_page: int):
        for page_number in range(start_page, end_page):
            fail_count = 0
            while True:
                try:
                    image_response_dict = self.get_page_images(page_number)
                    break
                except:
                    if fail_count > 1000:
                        return
                    print("Error at page_number {}".format(page_number))
                    fail_count += 1
                    continue

            for image_dict in image_response_dict['images']:
                self.lock.acquire()
                if (self.max_images > 1) and (self.downloaded_count >= self.max_images):
                    self.lock.release()
                    # print("Called")
                    return
                else:
                    self.lock.release()
                image_url = self.download_the_image(image_dict)
                if image_dict['annotated'] and (not image_dict["delete_details"]["deleted"]):
                    self.annot_writer.writer.write_annotation(image_dict, image_url, self.cur_proc)
                    self.lock.acquire()
                    self.downloaded_count += 1
                    self.lock.release()
                elif image_dict["skip_details"]['is_skipped']:
                    self.lock.acquire()
                    self.skip_count += 1
                    self.lock.release()
                # print(image_url)

    def start_multiprocess_download(self, num_of_pages):
        num_of_threads = self.config["NUM_THREADS"]
        if num_of_threads > num_of_pages:
            num_of_threads = num_of_pages
        download_divisions = np.linspace(1, num_of_pages, num_of_threads + 1,
                                        dtype = int)
        download_divisions[-1] += 1
        my_process = []
        for i in range(num_of_threads):
            start_page = download_divisions[i]
            end_page = download_divisions[i + 1]
            p = threading.Thread(target = self.multiprocess_download_func,
                                        args = (start_page, end_page))
            my_process.append(p)
        for p in my_process:
            p.start()
        for i,p in enumerate(my_process):
            p.join()

    def get_filtered_dataset_dict(self) -> List[Tuple[str, Dict]]:
        dataset_infos = []
        if self.use_dataset_api:
            for dataset_dict in self.fetched_datasets:
                try:
                    compare_str = "Segmentation" if dataset_dict['category_Type'] in \
                        ['Polygon', 'MagicWand'] else dataset_dict['category_Type']
                    if (dataset_dict[self.dataset_query_id] in self.datasets):
                        if compare_str == self.type_of_data:
                            dataset_infos.append((dataset_dict[self.dataset_query_id],dataset_dict))
                        else:
                            raise ValueError("{} dataset type is {} but expected {} according to config".format(dataset_dict[self.dataset_query_id], compare_str, self.type_of_data))
                except KeyError as e:
                    message = "While hitting found that dataset {} has no category_Type key (Problem in NM database)".format(dataset_dict[self.dataset_query_id])
                    print(message)
                    continue
        else:
            for dataset_ins in self.datasets:
                try:
                    data_info = self.get_dataset_info(int(dataset_ins))
                    compare_str = "Segmentation" if data_info['dataset']['category_Type'] in \
                        ['Polygon', 'MagicWand'] else data_info['dataset']['category_Type']
                    if compare_str == self.type_of_data:
                        dataset_infos.append((data_info['dataset'][self.dataset_query_id], data_info['dataset']))
                    else:
                        raise ValueError("{} dataset type is {} but expected {} according to config".format(data_info['dataset'][self.dataset_query_id], compare_str, self.type_of_data))
                except KeyError as e:
                    print("Error while getting dataset info of {}".format(dataset_ins))
                    sys.exit(0)
        found_datasets = [i[0] for i in dataset_infos]
        for data_name in self.datasets:
            if data_name not in found_datasets:
                exit_message = "Cannot find info for dataset {} from the user account".format(data_name)
                raise ValueError(exit_message)
        return dataset_infos


    def get_datasets(self):
        if self.use_dataset_api:
            url = self.DATASET_FETCH_URL
            headers = {
                'token': self.login_token
            }
            payload = {}
            response = requests.request("GET", url, headers = headers, 
                                                data = payload)
            return json.loads(response.text)
        return None

    def get_dataset_info(self, id):
        url = self.DATASET_FETCH_URL + "/{}/data?page=1&limit=3&folder=&file_name__icontains=&skip_details__is_skipped=false&order=file_name&route=dataset".format(id)
        headers = {'token': self.login_token}
        payload = {}
        response = requests.request("GET", url, headers = headers, 
                                                data = payload)
        return json.loads(response.text) 
    
    def _create_save_and_dataset_dir(self):
        if os.path.exists(self.save_dir):
            shutil.rmtree(self.save_dir)
        os.makedirs(self.images_dir)
        os.makedirs(self.annots_dir)
        os.makedirs(self.train_images_dir)
        os.makedirs(self.val_images_dir)

                
