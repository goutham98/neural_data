from utils.coco_writer import COCO_Format
from utils.pascal_writer import Pascal_VOC_Format
import sys

class Annotation_Writer():
    def __init__(self, anno_format, save_annot_dir, type_of_data,
                    dataset_name, val_ratio):
        self.anno_format = anno_format
        if anno_format == 'coco':
            self.writer = COCO_Format(save_annot_dir, type_of_data,
                                        dataset_name, val_ratio)
        elif anno_format == 'pascal':
            self.writer = Pascal_VOC_Format(save_annot_dir, type_of_data,
                                        dataset_name, val_ratio)
        else:
            message = "Only COCO and PascalVOC format are supported."
            raise ValueError(message)