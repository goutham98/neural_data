import requests
import json

class Login():
    def __init__(self, config):
        self._login_token = None
        self.config = config
        self.USERNAME = self.config['USERNAME']
        self.PASSWORD = self.config['PASSWORD']
        self.LOADING = self.config['LOADING']
        self.LOGIN_URL = self.config['NEURAL_MARKER_SERVER_URL_PREFIX'] + \
                            self.config['LOGIN_ENDPOINT_SUFFIX']
    def perform_login(self):
        payload = "{\n\t\"username\": \"" + self.USERNAME
        payload += "\",\n\t\"password\": \"" + self.PASSWORD 
        payload += "\",\n\t\"loading\": false\n}"
        headers = {
            "Content-Type" : 'application/json'
        }
        response = requests.post(self.LOGIN_URL, 
                                headers = headers, 
                                data = payload)
        response_dict = json.loads(response.text)
        if response_dict['success']:
            self._login_token = response_dict['user']['mapped_token']