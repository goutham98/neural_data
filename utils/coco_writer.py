from utils.anno_format import Format_Writer

import json
import random
import math
import os
import shutil

class COCO_Format(Format_Writer):
    
    def __init__(self, annot_save_dir, type_of_data, dataset_name, val_ratio):
        super().__init__(annot_save_dir, type_of_data, dataset_name, val_ratio)
        self.complete_data_dict = {
            'images' : {},
            'annotations' : {},
            'categories' : self.category_ids
        }
        self.train_dict = {
            'images' : [],
            'annotations' : [],
            'categories' : [],
            'info' : {}
        }
        self.val_dict = {
            'images' : [],
            'annotations' : [],
            'categories' : [],
            'info' : {}
        }
    
    def write_annotation(self, image_dict, url, curr_no):
        if self.type_of_data == 'Rectangle':
            self._write_annotation_for_detection(image_dict, url, curr_no)
        elif self.type_of_data == 'Segmentation':
            self._write_annotation_for_segmentation(image_dict, url, curr_no)
        elif self.type_of_data == 'Classification':
            self._write_annotation_for_classification(image_dict, url, curr_no)
        elif self.type_of_data == 'Keypoints':
            self._write_annotation_for_keypoints(image_dict, url, curr_no)
    
    def create_image_info(self, image_dict, url):
        image_info = {
            'file_name' : image_dict['file_name'],
            'id' : image_dict['id'],
            'width' : image_dict['width'],
            'height' : image_dict['height'],
            'nm_url' : url
        }
        return image_info

    def create_annotation_info(self, image_dict, annot, num):
        cat_name = annot['category_id']
        change_cat, _ = self.find_cat_present(cat_name, self.merge_categories)
        if change_cat:
            cat_name = change_cat
            cat_id = self.category_names_dict[cat_name]
        else: 
            cat_id = self.category_names_dict[cat_name]
        annot_info = {
            'image_id' : image_dict['id'],
            'category_id' : cat_id,
            'id' : self.global_annot_id,
            'bbox' : annot['bbox'],
            'area' : annot['area']
        }
        self.global_annot_id += 1
        return annot_info

    def _write_annotation_for_detection(self, image_dict, url, curr_no):
        image_info = self.create_image_info(image_dict, url)
        self.complete_data_dict['images'][image_dict['id']] = image_info
        annotations = image_dict['annotations']
        self.complete_data_dict['annotations'][image_dict['id']] = []
        if image_dict['annotated'] and len(image_dict['annotations']):
            for annot in annotations:
                annot_info = self.create_annotation_info(image_dict, annot, curr_no)
                self.complete_data_dict['annotations'][image_dict['id']]\
                                                        .append(annot_info)

    def _write_annotation_for_segmentation(self, image_dict, url, curr_no):
        pass

    def _write_annotation_for_classification(self, image_dict, url, curr_no):
        pass

    def _write_annotation_for_keypoints(self, image_dict, url, curr_no):
        pass

    def split_and_write(self, write_file_path,images_dir, train_dir, val_dir,dataset_info, skipped,seed=None):
        if seed:
            random.seed(seed)
        images = list(self.complete_data_dict['images'].keys())
        print("Total images from all Dataset -> {}".format(len(images)))
        total_annot = [len(i) for i in self.complete_data_dict['annotations'].values()]
        print("Total annotations from all images -> {}".format(sum(total_annot)))
        print("Number of skipped images due to no annotations -> {}".format(skipped))
        random.shuffle(images)
        number_of_validation_images = math.ceil(len(images) * self.split_data_ratio)
        for image_num, img_id in enumerate(images):
            if image_num != len(images) - number_of_validation_images:
                self.train_dict['images'].append(self.complete_data_dict['images'][img_id])
                for per_image_annot in self.complete_data_dict['annotations'][img_id]:
                    self.train_dict['annotations'].append(per_image_annot)
                shutil.move(os.path.join(images_dir, self.complete_data_dict['images'][img_id]['file_name']), train_dir)
                continue
            break
        print("No. of train images -> {}, No. of annotations for training -> {}".format(len(self.train_dict['images']), len(self.train_dict['annotations'])))
        self.train_dict['categories'] = self.complete_data_dict['categories']
        self.train_dict['info']['name'] = "_".join([str(i[1]["name"]) for i in dataset_info])
        self.train_dict['info']['org_id'] = "_".join([str(i[1]['organisation_id']['$oid']) for i in dataset_info]) 
        self.train_dict['info']['dataset_id'] = "_".join([str(i[1]["id"]) for i in dataset_info])
        self.train_dict['info']['owner'] = "_".join([str(i[1]["owner"]) for i in dataset_info])
        self.train_dict['info']['created_date'] = "_".join([str(i[1]['created_date']['$date']) for i in dataset_info])
        self.train_dict['info']['category'] = dataset_info[0][1]['category_Type']
        with open(os.path.join(write_file_path, "train.json"), 'w') as train_json:
            train_str = json.dumps(self.train_dict, indent = 4)
            train_json.write(train_str)
        for img_id in images[(len(images) - number_of_validation_images):]:
            self.val_dict['images'].append(self.complete_data_dict['images'][img_id])
            for per_image_annot in self.complete_data_dict['annotations'][img_id]:
                self.val_dict['annotations'].append(per_image_annot)
            shutil.move(os.path.join(images_dir, self.complete_data_dict['images'][img_id]['file_name']), val_dir)
        print("No. of val images -> {}, No. of annotations for validation -> {}".format(len(self.val_dict['images']), len(self.val_dict['annotations'])))
        self.val_dict['categories'] = self.complete_data_dict['categories']
        self.val_dict['info']['name'] = "_".join([str(i[1]["name"]) for i in dataset_info])
        self.val_dict['info']['org_id'] = "_".join([str(i[1]['organisation_id']['$oid']) for i in dataset_info]) 
        self.val_dict['info']['dataset_id'] = "_".join([str(i[1]["id"]) for i in dataset_info])
        self.val_dict['info']['owner'] = "_".join([str(i[1]["owner"]) for i in dataset_info])
        self.val_dict['info']['created_date'] = "_".join([str(i[1]['created_date']['$date']) for i in dataset_info])
        self.val_dict['info']['category'] = dataset_info[0][1]['category_Type']
        with open(os.path.join(write_file_path, "val.json"), 'w') as val_json:
            val_str = json.dumps(self.val_dict, indent = 4)
            val_json.write(val_str)
        shutil.rmtree(images_dir)