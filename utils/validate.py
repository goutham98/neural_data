import sys


def validate_args(config):
    success_flag = True
    if config["DATASET_NAME"]:
        if not (-1 < config["TASK_TYPE"] < 4):
            print("Fatal Error: Parsing the 'type' command line args")
            print("The valid value of 'type' ranges from 0 to 3 (inclusive)")
            print("0 -> Object Detection (Default)")
            print("1 -> Segmentation")
            print("2 -> Classification")
            print("3 -> Keypoints")
            success_flag = False

        if config["ANNOTATION_FORMAT"] not in ['pascal', 'coco']:
            print("Fatal Error: Parsing the 'annotation_format argument'")
            print("It should either be the Pascal or the COCO format.")
            success_flag = False
    else:
        success_flag = False
    # TODO: write the validation checks for the Training arguments
    return success_flag