from utils.anno_format import Format_Writer
class Pascal_VOC_Format(Format_Writer):
    def __init__(self, annot_save_dir, type_of_data, dataset_name, val_ratio):
        super().__init__(annot_save_dir, type_of_data, dataset_name, val_ratio)
        

    def write_annotation(self):
        if self.type_of_data == 'Rectangle':
            self._write_annotation_for_detection()
        elif self.type_of_data == 'Segmentation':
            self._write_annotation_for_segmentation()
        elif self.type_of_data == 'Classification':
            self._write_annotation_for_classification()
        elif self.type_of_data == 'Keypoints':
            self._write_annotation_for_keypoints()
    
    def _write_annotation_for_detection(self):
        pass

    def _write_annotation_for_segmentation(self):
        pass

    def _write_annotation_for_classification(self):
        pass

    def _write_annotation_for_keypoints(self):
        pass