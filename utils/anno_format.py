class Format_Writer():
    def __init__(self, annot_save_dir, type_of_data, dataset_name, val_ratio):
        self.annot_save_dir = annot_save_dir
        self.type_of_data = type_of_data
        self.dataset_name = dataset_name
        self.split_data_ratio = val_ratio
        self.global_annot_id = 1
        self.category_ids = []
        self.category_ids_dict = {}
        self.category_names_dict = {}
        self.dataset_mapping = None

    def write_annotation(self):
        pass
    
    @staticmethod
    def find_cat_present(name, cat_dict):
        for i, merge_cats in enumerate(cat_dict):
            if name in merge_cats[0]:
                return merge_cats[1], i
        return None, None

    def populate_category_ids(self, possible_categories, ind_dataset_map, merge_categories):
        self.merge_categories = merge_categories
        if merge_categories: filled_index = []
        for iter_cat_name, iter_cat_info in possible_categories.items():
            cat_name = iter_cat_name
            cat_info = iter_cat_info
            if merge_categories:
                new_cat, index = self.find_cat_present(cat_name, merge_categories)
                if new_cat and (index not in filled_index):
                    cat_name = new_cat
                    cat_info["super_category"] = "merged_cat"
                    cat_info["id"] = possible_categories[iter_cat_name]["id"]
                    filled_index.append(index)
                elif new_cat and (index in filled_index):
                    continue
            self.category_ids.append({'id' : cat_info['id'],
                                'name' : cat_name,
                                'super_category' : cat_info['super_category']})
            self.category_ids_dict[cat_info['id']] = cat_name
            self.category_names_dict[cat_name] = cat_info['id']
        self.dataset_mapping = ind_dataset_map
        if merge_categories: print("Categories after merging are {}".format(list(self.category_names_dict.keys())))