import argparse
import sys
import yaml
from utils.validate import validate_args
from Download_API.downloader import Downloader
from utils.login import Login
import time
import os

class Entry_To_API:
    def __init__(self, config_path):
        self.config = yaml.full_load(open(config_path))
        if not validate_args(self.config):
            raise RuntimeError("Error occured while validating the data config")
        self.login_token = None
        self._login_object = Login(self.config)
        self._login()
    
    def dataset_download(self):
        self.downloader = Downloader(self.login_token, self.config)
        start = time.time()
        self.downloader.start_download()
        print("Time: ", time.time() - start)

    def _login(self):
        self._login_object.perform_login()
        if self._login_object._login_token != None:
            self.login_token = self._login_object._login_token
        else:
            raise RuntimeError("Login failed")


if __name__ == "__main__":
    prev_dir = os.sep.join(os.path.dirname(os.path.abspath(__file__)).split(os.sep)[:-1])
    neural_marker = Entry_To_API(os.path.join(prev_dir, "data_config.yaml"))
    neural_marker.dataset_download()